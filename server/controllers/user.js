'use strict';
var log = require('../libs/log')(module);
var UserModel = require('../models/user');
var _ = require('lodash');

module.exports.addUser = function (req, res, next) {
  var newUser = new UserModel({
    userName: req.body.userName,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email
  });

  return newUser
    .trySave()
    .then(function () {
      return res.status(201).json({status: 'OK', user: newUser});
    })
    .catch(next);
};

module.exports.usersForPagination = function (req, res, next) {
  return UserModel
    .find({}, null, {skip: Number(req.query.skipPage), limit: Number(req.query.itemsOnPage)})
    .then(function (users) {
      return UserModel
        .count({})
        .then(function (count) {
          return res.json({status: 'OK', users: users, count: count});
        });
    })
    .catch(next);
};

module.exports.getUsersByPageNumber = function (req, res, next) {
  var configPagination = {
    limit: 5,
    page: req.params.page ? Number(req.params.page) - 1 : 1
  };
  return UserModel
    .find({}, null, {skip: Number(configPagination.limit * configPagination.page), limit: Number(configPagination.limit)})
    .then(function (users) {
      return UserModel
        .count({})
        .then(function (count) {
          return res.json({status: 'OK', users: users, count: count, size: configPagination.limit});
        });
    })
    .catch(next);
};

module.exports.updateUser = function (req, res, next) {
  return UserModel
    .update(
      {_id: req.params.userId},
      {$set: _.pick(req.body, ['userName', 'firstName', 'lastName', 'email'])})
    .then(function (modifiedUser) {
      return res.json({status: 'OK', modifiedUser: modifiedUser});
    })
    .catch(next);
};

module.exports.allUsers = function (req, res, next) {
  return UserModel
    .find({})
    .then(function (users) {
      return res.json({status: 'OK', users: users});
    })
    .catch(next);
};

module.exports.usersGroup = function (req, res, next) {
  return UserModel
    .find({groupId: req.params.groupId})
    .then(function (users) {
      return res.json({status: 'OK', users: users});
    })
    .catch(next);
};

module.exports.userById = function (req, res, next) {
  return res.json({status: 'OK', user: req.user});
};

module.exports.deleteUser = function (req, res, next) {
  return UserModel
    .remove({_id: req.params.userId})
    .then(function (modifiedUsers) {
      return res.send({status: 'OK', modifiedUsers: modifiedUsers});
    })
    .catch(next);
};

module.exports.deleteGroupFromUser = function (req, res, next) {
  var userId = req.params.userId;
  var groupId = req.params.groupId;

  return UserModel
    .update({_id: userId}, {$pull: {groupId: groupId}})
    .then(function (user) {
      return res.json({status: 'OK', users: user});
    })
    .catch(next);
};

module.exports.searchUsers = function (req, res, next) {
  var searchParams = req.params.searchParams;

  return UserModel.find({
    $or: [
      {userName: {$regex: searchParams}},
      {firstName: {$regex: searchParams}},
      {lastName: {$regex: searchParams}},
      {email: {$regex: searchParams}}
    ]
  })
    .limit(10)
    .then(function (users) {
      return res.json({status: 'OK', usersSearchResult: users});
    })
    .catch(next);
};

module.exports.addGroupForUser = function (req, res, next) {
  return UserModel
    .update({_id: req.user._id}, {$addToSet: {groupId: req.params.groupId}})
    .then(function (user) {
      return res.json({status: 'OK', users: user});
    })
    .catch(next);
};
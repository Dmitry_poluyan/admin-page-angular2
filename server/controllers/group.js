'use strict';
var log = require('../libs/log')(module);
var GroupModel = require('../models/group');
var UserModel = require('../models/user');
var _ = require('lodash');

module.exports.addGroup = function (req, res, next) {
  var newGroup = new GroupModel({
    groupName: req.body.groupName,
    groupTitle: req.body.groupTitle
  });

  return newGroup
    .trySave()
    .then(function () {
      return res.status(201).json({status: 'OK', group: newGroup});
    })
    .catch(next);
};

module.exports.groupsForPagination = function (req, res, next) {
  return GroupModel
    .find({}, null, {skip: Number(req.query.skipPage), limit: Number(req.query.itemsOnPage)})
    .then(function (groups) {
      return GroupModel
        .count({})
        .then(function (count) {
          return res.json({status: 'OK', groups: groups, count: count});
        });
    })
    .catch(next);
};

module.exports.allGroups = function (req, res, next) {
  return GroupModel
    .find({})
    .then(function (groups) {
      return res.json({status: 'OK', groups: groups});
    })
    .catch(next);
};

module.exports.allGroupNames = function (req, res, next) {
  return GroupModel
    .find({}, {groupName: 1})
    .then(function (groups) {
      return res.json({status: 'OK', groups: groups});
    })
    .catch(next);
};

module.exports.updateGroup = function (req, res, next) {
  return GroupModel
    .update(
      {_id: req.params.groupId},
      {$set: _.pick(req.body, ['groupName', 'groupTitle'])})
    .then(function (modifiedGroup) {
      return res.json({status: 'OK', modifiedGroup: modifiedGroup});
    })
    .catch(next);
};

module.exports.groupById = function (req, res, next) {
  return res.json({status: 'OK', group: req.group});
};

module.exports.deleteGroup = function (req, res, next) {
  return GroupModel
    .remove({_id: req.params.groupId})
    .then(function (modifiedGroups) {
      return res.send({status: 'OK', modifiedGroups: modifiedGroups});
    })
    .catch(next);
};

module.exports.searchGroups = function (req, res, next) {
  var searchParams = req.params.searchParams;

  return GroupModel
    .find({
      $or: [
        {groupName: {$regex: searchParams}},
        {groupTitle: {$regex: searchParams}}
      ]
    })
    .limit(10)
    .then(function (groups) {
      return res.json({status: 'OK', groupsSearchResult: groups});
    })
    .catch(next);
};

module.exports.groupsUser = function (req, res, next) {
  return UserModel
    .findById(req.params.userId)
    .populate('groupId')
    .then(function (users) {
      return res.json({status: 'OK', groups: users.groupId});
    })
    .catch(next);
};
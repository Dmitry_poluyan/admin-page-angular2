'use strict';
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

var Schema = mongoose.Schema;

var User = new Schema({
  userName: {
    type: String,
    unique: 'The user name exists',
    required: 'The user name required',
    lowercase: true,
    trim: true,
    validate: [/^[a-zA-Z0-9-.]{4,20}$/, 'You must enter: 4 ~ 20 characters (Latin alphabet, digits, ".", "-")']
  },
  firstName: {
    type: String,
    required: 'The first name required',
    trim: true,
    validate: [/^[a-zA-Z\s-]{2,20}$/, 'You must enter: 2 ~ 20 characters (only letters, the space character and "-")']
  },
  lastName: {
    type: String,
    required: 'The last name required',
    trim: true,
    validate: [/^[a-zA-Z]{2,20}$/, 'You must enter: 2 ~ 20 characters (only letters)']
  },
  email: {
    type: String,
    unique: 'The user email exists',
    required: 'The user email required',
    lowercase: true,
    trim: true,
    minlength: 3,
    maxlength: 30,
    validate: [/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/, 'You must enter: 3 ~ 30 characters in the format of email']
  },
  groupId: [{
    type: Schema.Types.ObjectId,
    ref: 'Group'
  }]
});

User.plugin(require('mongoose-beautiful-unique-validation'));

var UserModel = mongoose.model('User', User);

module.exports = UserModel;
'use strict';
var mongoose    = require('mongoose');
var config = require('../config');
var log = require('../libs/log')(module);

mongoose.Promise = require('bluebird');

mongoose.connect(config.get('db:connection'));
var db = mongoose.connection;

db.on('error', function (err) {
    log.error('connection error:', err.message);
});
db.once('open', function () {
    log.info('Connected to DB!');
});
console.log('111');

module.exports = db;
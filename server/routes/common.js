'use strict';
var commonCtrl = require('../controllers/common.js');
var log = require('../libs/log')(module);

var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
  log.info('Time: ', Date.now());
  next();
});

router.get('/checkUnique/:model/:property/:value', commonCtrl.checkUnique);

module.exports = router;
'use strict';
var usersCtrl = require('../controllers/user.js');
var paramCtrl = require('../controllers/param.js');
var log = require('../libs/log')(module);

var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
  log.info('Time: ', Date.now());
  next();
});

router.param('user_id', paramCtrl.userById);

router.get('/usersForPagination', usersCtrl.usersForPagination);
router.get('/usersByPageNumber/:page', usersCtrl.getUsersByPageNumber);
router.get('/', usersCtrl.allUsers);
router.get('/:user_id', usersCtrl.userById);
router.get('/usersGroup/:groupId', usersCtrl.usersGroup);
router.get('/search/:searchParams', usersCtrl.searchUsers);

router.post('/', usersCtrl.addUser);

router.put('/:userId', usersCtrl.updateUser);
router.put('/:user_id/addGroup/:groupId', usersCtrl.addGroupForUser);
router.put('/:userId/deleteGroup/:groupId', usersCtrl.deleteGroupFromUser);

router.delete('/:userId', usersCtrl.deleteUser);

module.exports = router;
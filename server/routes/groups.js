'use strict';
var groupsCtrl = require('../controllers/group.js');
var paramCtrl = require('../controllers/param.js');
var log = require('../libs/log')(module);

var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
  log.info('Time: ', Date.now());
  next();
});

router.param('group_id', paramCtrl.groupById);

router.get('/groupsForPagination', groupsCtrl.groupsForPagination);
router.get('/', groupsCtrl.allGroups);
router.get('/:group_id', groupsCtrl.groupById);
router.get('/groupsUser/:userId', groupsCtrl.groupsUser);
router.get('/search/:searchParams', groupsCtrl.searchGroups);

router.post('/', groupsCtrl.addGroup);

router.put('/:groupId', groupsCtrl.updateGroup);

router.delete('/:groupId', groupsCtrl.deleteGroup);

module.exports = router;
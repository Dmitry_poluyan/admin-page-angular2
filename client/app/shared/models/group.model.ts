export interface IGroup {
  _id:string;
  groupName:string;
  groupTitle:string;
}

export class Group implements IGroup {
  _id:string;
  groupName:string;
  groupTitle:string;

  constructor(group: any) {
    console.log('Group: constructor');

    group = group || {};

    this.groupName = group.groupName ? group.groupName : null;
    this.groupTitle = group.groupTitle ? group.groupTitle : null;
  }
}
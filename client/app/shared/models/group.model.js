"use strict";
var Group = (function () {
    function Group(group) {
        console.log('Group: constructor');
        group = group || {};
        this.groupName = group.groupName ? group.groupName : null;
        this.groupTitle = group.groupTitle ? group.groupTitle : null;
    }
    return Group;
}());
exports.Group = Group;
//# sourceMappingURL=group.model.js.map
"use strict";
var User = (function () {
    function User(user) {
        console.log('User: constructor');
        user = user || {};
        this.userName = user.userName ? user.userName : null;
        this.firstName = user.firstName ? user.firstName : null;
        this.lastName = user.lastName ? user.lastName : null;
        this.email = user.email ? user.email : null;
    }
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.model.js.map
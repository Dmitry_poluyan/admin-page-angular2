export interface IUser {
    _id:string;
    userName:string;
    firstName:string;
    lastName:string;
    email:string;
    groupId?:string[];
}

export class User implements IUser {
    _id:string;
    userName:string;
    firstName:string;
    lastName:string;
    email:string;
    groupId:string[];

    constructor(user: any) {
        console.log('User: constructor');
        
        user = user || {};

        this.userName = user.userName ? user.userName : null;
        this.firstName = user.firstName ? user.firstName : null;
        this.lastName = user.lastName ? user.lastName : null;
        this.email = user.email ? user.email : null;
    }
}
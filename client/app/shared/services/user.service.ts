import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {IUser} from '../models/user.model';
import {IGroup} from '../models/group.model';

@Injectable()
export class UserService {
  apiUrl:string;

  constructor(private http:Http) {
    console.log('UserService: constructor');

    this.apiUrl = 'http://localhost:3000/api/users/';
  }

  getUsers():Promise<IUser[]> {
    console.log('UserService: getUsers');

    return this.http
      .get(this.apiUrl)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json().users;
      })
      .catch(UserService.handleError);
  }

  getUsersByPageNumber(page:number):Promise<any> {
    console.log('UserService: getUsersByPageNumber');

    return this.http
      .get(`${this.apiUrl}usersByPageNumber/${page}`)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json();
      })
      .catch(UserService.handleError);
  }

  getUserById(id:string):Promise<IUser> {
    console.log('UserService: getUsers');

    return this.http
      .get(this.apiUrl + id)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json().user;
      })
      .catch(UserService.handleError);
  }

  createUser(user:IUser):Promise<IUser> {
    console.log('UserService: createUser');

    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers});
    let body = JSON.stringify(user);

    return this.http
      .post(this.apiUrl, body, options)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json().user;
      })
      .catch(UserService.handleError);
  }

  addGroupForUser(groupId:string, userId:string):Promise<any> {
    console.log('UserService: addGroupForUser');

    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers});
    let body = JSON.stringify({});
    let url = `${this.apiUrl}${userId}/addGroup/${groupId}`;

    return this.http
      .put(url, body, options)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json().users;
      })
      .catch(UserService.handleError);
  }

  getGroupUsersById(id:string):Promise<IUser[]> {
    console.log('UserService: getGroupUsersById');

    return this.http
      .get(this.apiUrl + 'usersGroup/' + id)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json().users;
      })
      .catch(UserService.handleError);
  }

  deleteGroupFromUser(groupId:string, userId:string):Promise<any> {
    console.log('UserService: deleteGroupFromUser');

    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers});
    let body = JSON.stringify({});
    let url = `${this.apiUrl}${userId}/deleteGroup/${groupId}`;

    return this.http
      .put(url, body, options)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json().users;
      })
      .catch(UserService.handleError);
  }

  saveUser(user:IUser):Promise<IUser> {
    console.log('UserService: saveUser');

    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers});
    let body = JSON.stringify(user);
    let url = `${this.apiUrl}/${user._id}`;

    return this.http
      .put(url, body, options)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json();
      })
      .catch(UserService.handleError);
  }

  deleteUserById(id:string):Promise<IUser> {
    console.log('UserService: deleteUser');

    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers});
    let url = `${this.apiUrl}/${id}`;

    return this.http
      .delete(url, options)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json();
      })
      .catch(UserService.handleError);
  }

  private static handleError(err:any):Promise<any> {
    console.log('UserService: handleError');

    let error = err.json();

    console.log('Error', error);

    return Promise.reject(error.message || error.messageError || error);
  }
}
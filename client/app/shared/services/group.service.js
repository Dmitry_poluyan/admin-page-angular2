"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var GroupService = (function () {
    function GroupService(http) {
        this.http = http;
        console.log('GroupService: constructor');
        this.apiUrl = 'http://localhost:3000/api/groups/';
    }
    GroupService.prototype.getGroups = function () {
        console.log('GroupService: getGroups');
        return this.http
            .get(this.apiUrl)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json().groups;
        })
            .catch(GroupService.handleError);
    };
    GroupService.prototype.getGroupById = function (id) {
        console.log('GroupService: getGroupById');
        return this.http
            .get(this.apiUrl + id)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json().group;
        })
            .catch(GroupService.handleError);
    };
    GroupService.prototype.getUserGroupsById = function (id) {
        console.log('GroupService: getUserGroupsById');
        return this.http
            .get(this.apiUrl + 'groupsUser/' + id)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json().groups;
        })
            .catch(GroupService.handleError);
    };
    GroupService.prototype.createGroup = function (group) {
        console.log('GroupService: createGroup');
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = JSON.stringify(group);
        return this.http
            .post(this.apiUrl, body, options)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json().user;
        })
            .catch(GroupService.handleError);
    };
    GroupService.prototype.saveGroup = function (group) {
        console.log('GroupService: saveGroup');
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = JSON.stringify(group);
        var url = this.apiUrl + "/" + group._id;
        return this.http
            .put(url, body, options)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json();
        })
            .catch(GroupService.handleError);
    };
    GroupService.prototype.deleteGroupById = function (id) {
        console.log('GroupService: deleteGroupById');
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var url = this.apiUrl + "/" + id;
        return this.http
            .delete(url, options)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json();
        })
            .catch(GroupService.handleError);
    };
    GroupService.handleError = function (err) {
        console.log('GroupService: handleError');
        console.log('Error', err.json());
        var error = err.json();
        return Promise.reject(error.message || error.messageError || error);
    };
    GroupService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], GroupService);
    return GroupService;
}());
exports.GroupService = GroupService;
//# sourceMappingURL=group.service.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var UserService = (function () {
    function UserService(http) {
        this.http = http;
        console.log('UserService: constructor');
        this.apiUrl = 'http://localhost:3000/api/users/';
    }
    UserService.prototype.getUsers = function () {
        console.log('UserService: getUsers');
        return this.http
            .get(this.apiUrl)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json().users;
        })
            .catch(UserService.handleError);
    };
    UserService.prototype.getUsersByPageNumber = function (page) {
        console.log('UserService: getUsersByPageNumber');
        return this.http
            .get(this.apiUrl + "usersByPageNumber/" + page)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json();
        })
            .catch(UserService.handleError);
    };
    UserService.prototype.getUserById = function (id) {
        console.log('UserService: getUsers');
        return this.http
            .get(this.apiUrl + id)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json().user;
        })
            .catch(UserService.handleError);
    };
    UserService.prototype.createUser = function (user) {
        console.log('UserService: createUser');
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = JSON.stringify(user);
        return this.http
            .post(this.apiUrl, body, options)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json().user;
        })
            .catch(UserService.handleError);
    };
    UserService.prototype.addGroupForUser = function (groupId, userId) {
        console.log('UserService: addGroupForUser');
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = JSON.stringify({});
        var url = "" + this.apiUrl + userId + "/addGroup/" + groupId;
        return this.http
            .put(url, body, options)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json().users;
        })
            .catch(UserService.handleError);
    };
    UserService.prototype.getGroupUsersById = function (id) {
        console.log('UserService: getGroupUsersById');
        return this.http
            .get(this.apiUrl + 'usersGroup/' + id)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json().users;
        })
            .catch(UserService.handleError);
    };
    UserService.prototype.deleteGroupFromUser = function (groupId, userId) {
        console.log('UserService: deleteGroupFromUser');
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = JSON.stringify({});
        var url = "" + this.apiUrl + userId + "/deleteGroup/" + groupId;
        return this.http
            .put(url, body, options)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json().users;
        })
            .catch(UserService.handleError);
    };
    UserService.prototype.saveUser = function (user) {
        console.log('UserService: saveUser');
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = JSON.stringify(user);
        var url = this.apiUrl + "/" + user._id;
        return this.http
            .put(url, body, options)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json();
        })
            .catch(UserService.handleError);
    };
    UserService.prototype.deleteUserById = function (id) {
        console.log('UserService: deleteUser');
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var url = this.apiUrl + "/" + id;
        return this.http
            .delete(url, options)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json();
        })
            .catch(UserService.handleError);
    };
    UserService.handleError = function (err) {
        console.log('UserService: handleError');
        var error = err.json();
        console.log('Error', error);
        return Promise.reject(error.message || error.messageError || error);
    };
    UserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map
import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {IGroup} from '../models/group.model';

@Injectable()
export class GroupService {
  apiUrl:string;

  constructor(private http:Http) {
    console.log('GroupService: constructor');

    this.apiUrl = 'http://localhost:3000/api/groups/';
  }

  getGroups():Promise<IGroup[]> {
    console.log('GroupService: getGroups');

    return this.http
      .get(this.apiUrl)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json().groups;
      })
      .catch(GroupService.handleError);
  }

  getGroupById(id:string):Promise<IGroup> {
    console.log('GroupService: getGroupById');

    return this.http
      .get(this.apiUrl + id)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json().group;
      })
      .catch(GroupService.handleError);
  }

  getUserGroupsById(id:string):Promise<IGroup[]> {
    console.log('GroupService: getUserGroupsById');

    return this.http
      .get(this.apiUrl + 'groupsUser/' + id)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json().groups;
      })
      .catch(GroupService.handleError);
  }

  createGroup(group:IGroup):Promise<IGroup> {
    console.log('GroupService: createGroup');

    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers});
    let body = JSON.stringify(group);

    return this.http
      .post(this.apiUrl, body, options)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json().user;
      })
      .catch(GroupService.handleError);
  }

  saveGroup(group:IGroup):Promise<IGroup> {
    console.log('GroupService: saveGroup');

    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers});
    let body = JSON.stringify(group);
    let url = `${this.apiUrl}/${group._id}`;

    return this.http
      .put(url, body, options)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json();
      })
      .catch(GroupService.handleError);
  }

  deleteGroupById(id:string):Promise<IGroup> {
    console.log('GroupService: deleteGroupById');

    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers});
    let url = `${this.apiUrl}/${id}`;

    return this.http
      .delete(url, options)
      .toPromise()
      .then((res:Response) => {
        console.log(res.json());

        return res.json();
      })
      .catch(GroupService.handleError);
  }

  private static handleError(err:any):Promise<any> {
    console.log('GroupService: handleError');
    console.log('Error', err.json());

    let error = err.json();

    return Promise.reject(error.message || error.messageError || error);
  }
}
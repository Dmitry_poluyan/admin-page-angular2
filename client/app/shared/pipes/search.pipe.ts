import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'searche'
})

export class SearchPipe implements PipeTransform {
  transform(value:any[], term:string) {
    return value.filter((item:any) => item.userName.startsWith(term));
  }
}
import {bootstrap}      from '@angular/platform-browser-dynamic';
import {HTTP_PROVIDERS} from '@angular/http';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {disableDeprecatedForms, provideForms} from '@angular/forms';
import {ToastyService, ToastyConfig} from 'ng2-toasty/ng2-toasty';

import {AppComponent} from './components/app.component';
import {appRouterProviders}   from './components/app.routes';

bootstrap(AppComponent, [
  disableDeprecatedForms(),
  provideForms(),
  appRouterProviders,
  HTTP_PROVIDERS,
  ToastyService,
  ToastyConfig,
  {provide: LocationStrategy, useClass: HashLocationStrategy}
])
  .catch(err => console.error(err));

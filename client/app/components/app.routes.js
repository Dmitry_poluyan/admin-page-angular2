"use strict";
var router_1 = require('@angular/router');
var admin_routes_1 = require('./admin/admin.routes');
var not_found_component_1 = require('./common/not.found/not.found.component');
var routes = [
    {
        path: '',
        redirectTo: '/admin/users',
        pathMatch: 'full'
    }
].concat(admin_routes_1.adminRoutes, [
    { path: '**', component: not_found_component_1.PageNotFoundComponent }
]);
exports.appRouterProviders = [
    router_1.provideRouter(routes)
];
//# sourceMappingURL=app.routes.js.map
import {Component} from '@angular/core';

@Component({
  templateUrl: 'app/components/common/not.found/not.found.component.html',
  styleUrls: ['app/components/common/not.found/not.found.component.css'],
})

export class PageNotFoundComponent {
  constructor() {
    console.log('PageNotFoundComponent: constructor');
  }
}
import {Component, Output, OnInit, EventEmitter} from '@angular/core';

@Component({
  selector: 'search-box',
  templateUrl: 'app/components/common/search/search.component.html',
  styleUrls: ['app/components/common/search/search.component.css']
})

export class SearchComponent implements OnInit{
  @Output() updated:EventEmitter<string>;

  constructor() {
    console.log('SearchComponent: constructor');

    this.updated = new EventEmitter<string>();
  }

  ngOnInit() {
    console.log('SearchComponent: ngOnInit');
    
    this.updated.emit('');
  }

  inputValueUpdate(value:string):void{
    console.log('SearchComponent: inputValueUpdate');

    this.updated.emit(value)
  }

  clearInputValue(input: any){
    console.log('SearchComponent: clearInputValue');
    
    input.value = '';
    this.updated.emit('');
  }
}
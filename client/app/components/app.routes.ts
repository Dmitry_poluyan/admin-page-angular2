import {provideRouter, RouterConfig}  from '@angular/router';

import {adminRoutes}  from './admin/admin.routes';
import {PageNotFoundComponent}  from './common/not.found/not.found.component';

const routes:RouterConfig = [
  {
    path: '',
    redirectTo: '/admin/users',
    pathMatch: 'full'
  },
  ...adminRoutes,
  {path: '**', component: PageNotFoundComponent}
];

export const appRouterProviders = [
  provideRouter(routes)
];
import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES}  from '@angular/router';
import '../shared/rxjs-extensions';
import {Toasty} from 'ng2-toasty/ng2-toasty';

@Component({
  selector: 'my-app',
  templateUrl: 'app/components/app.component.html',
  styleUrls: ['app/components/app.component.css'],
  directives: [ROUTER_DIRECTIVES, Toasty]
})

export class AppComponent {
  constructor() {
    console.log('AppComponent: constructor');
  }
}
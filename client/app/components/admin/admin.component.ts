import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES}  from '@angular/router';

@Component({
  selector: 'admin-page',
  templateUrl: 'app/components/admin/admin.component.html',
  styleUrls: ['app/components/admin/admin.component.css'],
  directives: [ROUTER_DIRECTIVES]
})

export class AdminComponent {
  constructor() {
    console.log('AdminComponent: constructor');
  }
}
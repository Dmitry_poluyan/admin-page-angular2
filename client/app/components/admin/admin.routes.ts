import {RouterConfig}       from '@angular/router';

import {AdminComponent} from './admin.component';
import {UsersComponent} from './users/users.component';
import {GroupsComponent} from './groups/groups.component';
import {userRouter} from './users/user.routes';
import {groupRouter} from './groups/group.router';

export const adminRoutes:RouterConfig = [
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: '/admin/users',
        pathMatch: 'full'
      },
      {
        path: 'users',
        component: UsersComponent
      },
      {
        path: 'groups',
        component: GroupsComponent
      },
      ...userRouter,
      ...groupRouter
    ]
  }
];

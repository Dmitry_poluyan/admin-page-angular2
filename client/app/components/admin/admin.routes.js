"use strict";
var admin_component_1 = require('./admin.component');
var users_component_1 = require('./users/users.component');
var groups_component_1 = require('./groups/groups.component');
var user_routes_1 = require('./users/user.routes');
var group_router_1 = require('./groups/group.router');
exports.adminRoutes = [
    {
        path: 'admin',
        component: admin_component_1.AdminComponent,
        children: [
            {
                path: '',
                redirectTo: '/admin/users',
                pathMatch: 'full'
            },
            {
                path: 'users',
                component: users_component_1.UsersComponent
            },
            {
                path: 'groups',
                component: groups_component_1.GroupsComponent
            }
        ].concat(user_routes_1.userRouter, group_router_1.groupRouter)
    }
];
//# sourceMappingURL=admin.routes.js.map
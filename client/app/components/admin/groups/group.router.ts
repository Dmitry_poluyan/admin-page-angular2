import {RouterConfig} from '@angular/router';

import {CreateGroupComponent} from './create.group/create.group.component';
import {profileRouter} from './group.profile/profile.router';

export const groupRouter:RouterConfig = [
  {
    path: 'createGroup',
    component: CreateGroupComponent
  },
  ...profileRouter
];
import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ROUTER_DIRECTIVES}  from '@angular/router';

import {IGroup} from '../../../../shared/models/group.model';

@Component({
  selector: 'groups-table',
  templateUrl: 'app/components/admin/groups/groups.table/groups.table.component.html',
  styleUrls: ['app/components/admin/groups/groups.table/groups.table.component.css'],
  directives: [ROUTER_DIRECTIVES]
})

export class GroupsTableComponent {
  @Input() groups:IGroup[];
  @Input() showDeleteBtn:boolean;
  @Output() deletedItem:EventEmitter<IGroup>;

  constructor() {
    console.log('GroupsTableComponent: constructor');

    this.deletedItem = new EventEmitter<IGroup>();
  }

  deleteItem(group:IGroup) {
    if (group && confirm('Are you sure?')) {
      this.deletedItem.emit(group);
    }
  }
}
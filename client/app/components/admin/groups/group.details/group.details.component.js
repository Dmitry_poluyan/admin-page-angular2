"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var group_model_1 = require('../../../../shared/models/group.model');
var group_service_1 = require('../../../../shared/services/group.service');
var GroupDetailsComponent = (function () {
    function GroupDetailsComponent(route, router, groupService) {
        this.route = route;
        this.router = router;
        this.groupService = groupService;
        console.log('GroupDetailsComponent: constructor');
        this.group = new group_model_1.Group({});
    }
    GroupDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('GroupDetailsComponent: ngOnInit');
        this.sub = this.router
            .routerState
            .parent(this.route)
            .params
            .subscribe(function (params) {
            var id = params['id'];
            _this.groupService
                .getGroupById(id)
                .then(function (group) { return _this.group = group; });
        });
    };
    GroupDetailsComponent.prototype.ngOnDestroy = function () {
        console.log('GroupDetailsComponent: ngOnDestroy');
        this.sub.unsubscribe();
    };
    GroupDetailsComponent = __decorate([
        core_1.Component({
            selector: 'group-details',
            templateUrl: 'app/components/admin/groups/group.details/group.details.component.html',
            styleUrls: ['app/components/admin/groups/group.details/group.details.component.css'],
            providers: [group_service_1.GroupService]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, group_service_1.GroupService])
    ], GroupDetailsComponent);
    return GroupDetailsComponent;
}());
exports.GroupDetailsComponent = GroupDetailsComponent;
//# sourceMappingURL=group.details.component.js.map
import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {IGroup, Group} from '../../../../shared/models/group.model';
import {GroupService} from '../../../../shared/services/group.service';

@Component({
  selector: 'group-details',
  templateUrl: 'app/components/admin/groups/group.details/group.details.component.html',
  styleUrls: ['app/components/admin/groups/group.details/group.details.component.css'],
  providers: [GroupService]
})

export class GroupDetailsComponent implements OnInit, OnDestroy {
  group:IGroup;
  sub:any;

  constructor(private route:ActivatedRoute,
              private router:Router,
              private groupService:GroupService) {
    console.log('GroupDetailsComponent: constructor');

    this.group = new Group({});
  }

  ngOnInit() {
    console.log('GroupDetailsComponent: ngOnInit');

    this.sub = this.router
      .routerState
      .parent(this.route)
      .params
      .subscribe(params => {
        let id = params['id'];

        this.groupService
          .getGroupById(id)
          .then((group:IGroup) => this.group = group);
      });
  }

  ngOnDestroy() {
    console.log('GroupDetailsComponent: ngOnDestroy');

    this.sub.unsubscribe();
  }
}
import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {ToastyService} from 'ng2-toasty/ng2-toasty';

import {IGroup, Group} from '../../../../shared/models/group.model';
import {GroupService} from '../../../../shared/services/group.service';
import {GroupFormComponent} from '../group.form/group.form.component';

@Component({
  selector: 'create-group-page',
  templateUrl: 'app/components/admin/groups/create.group/create.group.component.html',
  styleUrls: ['app/components/admin/groups/create.group/create.group.component.css'],
  providers: [GroupService],
  directives: [GroupFormComponent]
})

export class CreateGroupComponent {
  group:IGroup;
  nameSubmitBtn:string;

  constructor(private groupService:GroupService,
              private toastyService:ToastyService,
              private router:Router) {
    console.log('CreateGroupComponent: constructor');

    this.group = new Group({});
    this.nameSubmitBtn = 'Create';
  }

  onGroupCreated(group:IGroup):void {
    console.log('CreateGroupComponent: onGroupCreated');

    if (group) {
      this.groupService
        .createGroup(this.group)
        .then(() => {
          this.toastyService.success({
            title: 'Success',
            msg: 'Group added',
            theme: 'bootstrap'
          });

          this.group = new Group({});
          this.goToGroupPage();
        }).catch(error => {

        this.toastyService.warning({
          title: 'Warning',
          msg: error ? error : 'Data is invalid',
          theme: 'bootstrap'
        });
      });
    }
  }

  private goToGroupPage():void {
    console.log('CreateGroupComponent: goToGroupPage');

    this.router.navigate(['/admin/groups']);
  }
}
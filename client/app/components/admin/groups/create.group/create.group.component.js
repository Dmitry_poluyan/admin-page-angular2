"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ng2_toasty_1 = require('ng2-toasty/ng2-toasty');
var group_model_1 = require('../../../../shared/models/group.model');
var group_service_1 = require('../../../../shared/services/group.service');
var group_form_component_1 = require('../group.form/group.form.component');
var CreateGroupComponent = (function () {
    function CreateGroupComponent(groupService, toastyService, router) {
        this.groupService = groupService;
        this.toastyService = toastyService;
        this.router = router;
        console.log('CreateGroupComponent: constructor');
        this.group = new group_model_1.Group({});
        this.nameSubmitBtn = 'Create';
    }
    CreateGroupComponent.prototype.onGroupCreated = function (group) {
        var _this = this;
        console.log('CreateGroupComponent: onGroupCreated');
        if (group) {
            this.groupService
                .createGroup(this.group)
                .then(function () {
                _this.toastyService.success({
                    title: 'Success',
                    msg: 'Group added',
                    theme: 'bootstrap'
                });
                _this.group = new group_model_1.Group({});
                _this.goToGroupPage();
            }).catch(function (error) {
                _this.toastyService.warning({
                    title: 'Warning',
                    msg: error ? error : 'Data is invalid',
                    theme: 'bootstrap'
                });
            });
        }
    };
    CreateGroupComponent.prototype.goToGroupPage = function () {
        console.log('CreateGroupComponent: goToGroupPage');
        this.router.navigate(['/admin/groups']);
    };
    CreateGroupComponent = __decorate([
        core_1.Component({
            selector: 'create-group-page',
            templateUrl: 'app/components/admin/groups/create.group/create.group.component.html',
            styleUrls: ['app/components/admin/groups/create.group/create.group.component.css'],
            providers: [group_service_1.GroupService],
            directives: [group_form_component_1.GroupFormComponent]
        }), 
        __metadata('design:paramtypes', [group_service_1.GroupService, ng2_toasty_1.ToastyService, router_1.Router])
    ], CreateGroupComponent);
    return CreateGroupComponent;
}());
exports.CreateGroupComponent = CreateGroupComponent;
//# sourceMappingURL=create.group.component.js.map
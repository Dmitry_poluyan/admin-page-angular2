"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var groups_table_component_1 = require('./groups.table/groups.table.component');
var group_service_1 = require('../../../shared/services/group.service');
var GroupsComponent = (function () {
    function GroupsComponent(groupService, router) {
        this.groupService = groupService;
        this.router = router;
        console.log('GroupsComponent: constructor');
        this.groups = [];
        this.showDeleteBtn = false;
    }
    GroupsComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('GroupsComponent: ngOnInit');
        this.groupService
            .getGroups()
            .then(function (groups) { return _this.groups = groups; });
    };
    GroupsComponent.prototype.goToAddGroupPage = function () {
        console.log('UsersComponent: goToAddUserPage');
        this.router.navigate(['/admin/createGroup']);
    };
    GroupsComponent = __decorate([
        core_1.Component({
            selector: 'groups-page',
            templateUrl: 'app/components/admin/groups/groups.component.html',
            styleUrls: ['app/components/admin/groups/groups.component.css'],
            directives: [groups_table_component_1.GroupsTableComponent],
            providers: [group_service_1.GroupService]
        }), 
        __metadata('design:paramtypes', [group_service_1.GroupService, router_1.Router])
    ], GroupsComponent);
    return GroupsComponent;
}());
exports.GroupsComponent = GroupsComponent;
//# sourceMappingURL=groups.component.js.map
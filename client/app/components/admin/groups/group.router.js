"use strict";
var create_group_component_1 = require('./create.group/create.group.component');
var profile_router_1 = require('./group.profile/profile.router');
exports.groupRouter = [
    {
        path: 'createGroup',
        component: create_group_component_1.CreateGroupComponent
    }
].concat(profile_router_1.profileRouter);
//# sourceMappingURL=group.router.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ng2_toasty_1 = require('ng2-toasty/ng2-toasty');
var group_service_1 = require('../../../../shared/services/group.service');
var GroupProfileComponent = (function () {
    function GroupProfileComponent(route, groupService, router, toastyService) {
        this.route = route;
        this.groupService = groupService;
        this.router = router;
        this.toastyService = toastyService;
        console.log('GroupProfileComponent: constructor');
    }
    GroupProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('GroupProfileComponent: ngOnInit');
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id'];
        });
    };
    GroupProfileComponent.prototype.deleteGroup = function () {
        var _this = this;
        console.log('GroupProfileComponent: deleteGroup');
        if (this.id && confirm('Are you sure?')) {
            this.groupService
                .deleteGroupById(this.id)
                .then(function () {
                _this.toastyService.success({
                    title: 'Success',
                    msg: 'Group deleted',
                    theme: 'bootstrap'
                });
                _this.goToGroupsPage();
            }).catch(function (error) {
                _this.toastyService.warning({
                    title: 'Warning',
                    msg: error ? error : 'Data is invalid',
                    theme: 'bootstrap'
                });
            });
        }
    };
    GroupProfileComponent.prototype.goToGroupsPage = function () {
        console.log('GroupProfileComponent: goToGroupsPage');
        this.router.navigate(['/admin/groups']);
    };
    GroupProfileComponent = __decorate([
        core_1.Component({
            selector: 'group-profile',
            templateUrl: 'app/components/admin/groups/group.profile/group.profile.component.html',
            styleUrls: ['app/components/admin/groups/group.profile/group.profile.component.css'],
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [group_service_1.GroupService]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, group_service_1.GroupService, router_1.Router, ng2_toasty_1.ToastyService])
    ], GroupProfileComponent);
    return GroupProfileComponent;
}());
exports.GroupProfileComponent = GroupProfileComponent;
//# sourceMappingURL=group.profile.component.js.map
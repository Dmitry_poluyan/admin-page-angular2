"use strict";
var group_profile_component_1 = require('../group.profile/group.profile.component');
var group_details_component_1 = require('../group.details/group.details.component');
var edit_group_component_1 = require('../edit.group/edit.group.component');
var group_users_component_1 = require('../group.users/group.users.component');
exports.profileRouter = [
    {
        path: 'groupProfile/:id',
        component: group_profile_component_1.GroupProfileComponent,
        children: [
            {
                path: '',
                redirectTo: 'details'
            },
            {
                path: 'details',
                component: group_details_component_1.GroupDetailsComponent
            },
            {
                path: 'edit',
                component: edit_group_component_1.EditGroupComponent
            },
            {
                path: 'groupUsers',
                component: group_users_component_1.GroupUsersComponent
            }
        ]
    }
];
//# sourceMappingURL=profile.router.js.map
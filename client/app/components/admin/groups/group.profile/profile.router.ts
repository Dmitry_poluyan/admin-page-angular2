import {RouterConfig} from '@angular/router';

import {GroupProfileComponent} from '../group.profile/group.profile.component';
import {GroupDetailsComponent} from '../group.details/group.details.component';
import {EditGroupComponent} from '../edit.group/edit.group.component';
import {GroupUsersComponent} from '../group.users/group.users.component';

export const profileRouter:RouterConfig = [
  {
    path: 'groupProfile/:id',
    component: GroupProfileComponent,
    children: [
      {
        path: '',
        redirectTo: 'details'
      },
      {
        path: 'details',
        component: GroupDetailsComponent
      },
      {
        path: 'edit',
        component: EditGroupComponent
      },
      {
        path: 'groupUsers',
        component: GroupUsersComponent
      }
    ]
  }
];

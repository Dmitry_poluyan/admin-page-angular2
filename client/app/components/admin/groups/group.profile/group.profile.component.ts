import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, ActivatedRoute, Router}  from '@angular/router';
import {ToastyService} from 'ng2-toasty/ng2-toasty';

import {GroupService} from '../../../../shared/services/group.service';

@Component({
  selector: 'group-profile',
  templateUrl: 'app/components/admin/groups/group.profile/group.profile.component.html',
  styleUrls: ['app/components/admin/groups/group.profile/group.profile.component.css'],
  directives: [ROUTER_DIRECTIVES],
  providers: [GroupService]
})

export class GroupProfileComponent {
  id:string;
  sub:any;

  constructor(private route:ActivatedRoute,
              private groupService:GroupService,
              private router:Router,
              private toastyService:ToastyService) {
    console.log('GroupProfileComponent: constructor');
  }

  ngOnInit() {
    console.log('GroupProfileComponent: ngOnInit');

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  deleteGroup() {
    console.log('GroupProfileComponent: deleteGroup');

    if (this.id && confirm('Are you sure?')) {
      this.groupService
        .deleteGroupById(this.id)
        .then(() => {
          this.toastyService.success({
            title: 'Success',
            msg: 'Group deleted',
            theme: 'bootstrap'
          });

          this.goToGroupsPage();

        }).catch(error => {

        this.toastyService.warning({
          title: 'Warning',
          msg: error ? error : 'Data is invalid',
          theme: 'bootstrap'
        });
      });
    }
  }

  goToGroupsPage() {
    console.log('GroupProfileComponent: goToGroupsPage');

    this.router.navigate(['/admin/groups']);
  }
}



"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ng2_toasty_1 = require('ng2-toasty/ng2-toasty');
var group_model_1 = require('../../../../shared/models/group.model');
var group_service_1 = require('../../../../shared/services/group.service');
var group_form_component_1 = require('../group.form/group.form.component');
var EditGroupComponent = (function () {
    function EditGroupComponent(route, router, groupService, toastyService) {
        this.route = route;
        this.router = router;
        this.groupService = groupService;
        this.toastyService = toastyService;
        console.log('EditGroupComponent: constructor');
        this.group = new group_model_1.Group({});
        this.nameSubmitBtn = 'Edit';
    }
    EditGroupComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('EditGroupComponent: ngOnInit');
        this.sub = this.router
            .routerState
            .parent(this.route)
            .params
            .subscribe(function (params) {
            _this.groupService
                .getGroupById(params['id'])
                .then(function (group) { return _this.group = group; });
        });
    };
    EditGroupComponent.prototype.onGroupEdited = function (group) {
        var _this = this;
        console.log('EditGroupComponent: onGroupEdited');
        if (group) {
            this.groupService
                .saveGroup(this.group)
                .then(function () {
                _this.toastyService.success({
                    title: 'Success',
                    msg: 'Group edited',
                    theme: 'bootstrap'
                });
                _this.goToBack();
            }).catch(function (error) {
                _this.toastyService.warning({
                    title: 'Warning',
                    msg: error ? error : 'Data is invalid',
                    theme: 'bootstrap'
                });
            });
        }
    };
    EditGroupComponent.prototype.ngOnDestroy = function () {
        console.log('EditGroupComponent: ngOnDestroy');
        this.sub.unsubscribe();
    };
    EditGroupComponent.prototype.goToBack = function () {
        console.log('EditGroupComponent: goBack');
        window.history.back();
    };
    EditGroupComponent = __decorate([
        core_1.Component({
            selector: 'group-details',
            templateUrl: 'app/components/admin/groups/edit.group/edit.group.component.html',
            styleUrls: ['app/components/admin/groups/edit.group/edit.group.component.css'],
            providers: [group_service_1.GroupService],
            directives: [group_form_component_1.GroupFormComponent]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, group_service_1.GroupService, ng2_toasty_1.ToastyService])
    ], EditGroupComponent);
    return EditGroupComponent;
}());
exports.EditGroupComponent = EditGroupComponent;
//# sourceMappingURL=edit.group.component.js.map
import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastyService} from 'ng2-toasty/ng2-toasty';

import {IGroup, Group} from '../../../../shared/models/group.model';
import {GroupService} from '../../../../shared/services/group.service';
import {GroupFormComponent} from '../group.form/group.form.component';

@Component({
  selector: 'group-details',
  templateUrl: 'app/components/admin/groups/edit.group/edit.group.component.html',
  styleUrls: ['app/components/admin/groups/edit.group/edit.group.component.css'],
  providers: [GroupService],
  directives: [GroupFormComponent]
})

export class EditGroupComponent implements OnInit, OnDestroy {
  group:IGroup;
  sub:any;
  nameSubmitBtn:string;

  constructor(private route:ActivatedRoute,
              private router:Router,
              private groupService:GroupService,
              private toastyService:ToastyService) {
    console.log('EditGroupComponent: constructor');

    this.group = new Group({});
    this.nameSubmitBtn = 'Edit';
  }

  ngOnInit() {
    console.log('EditGroupComponent: ngOnInit');

    this.sub = this.router
      .routerState
      .parent(this.route)
      .params
      .subscribe(params => {
        this.groupService
          .getGroupById(params['id'])
          .then((group:IGroup) => this.group = group);
      });
  }

  onGroupEdited(group:IGroup) {
    console.log('EditGroupComponent: onGroupEdited');

    if (group) {
      this.groupService
        .saveGroup(this.group)
        .then(() => {
          this.toastyService.success({
            title: 'Success',
            msg: 'Group edited',
            theme: 'bootstrap'
          });

          this.goToBack();
        }).catch((error:any) => {
        this.toastyService.warning({
          title: 'Warning',
          msg: error ? error : 'Data is invalid',
          theme: 'bootstrap'
        });
      });
    }
  }

  ngOnDestroy() {
    console.log('EditGroupComponent: ngOnDestroy');

    this.sub.unsubscribe();
  }

  goToBack() {
    console.log('EditGroupComponent: goBack');

    window.history.back();
  }
}
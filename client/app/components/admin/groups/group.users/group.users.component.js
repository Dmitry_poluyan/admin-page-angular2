"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var user_service_1 = require('../../../../shared/services/user.service');
var users_table_component_1 = require('../../users/users.table/users.table.component');
var GroupUsersComponent = (function () {
    function GroupUsersComponent(route, router, userService) {
        this.route = route;
        this.router = router;
        this.userService = userService;
        console.log('GroupUsersComponent: constructor');
    }
    GroupUsersComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('GroupUsersComponent: ngOnInit');
        this.sub = this.router.routerState.parent(this.route).params.subscribe(function (params) {
            _this.groupId = params['id'];
            _this.getGroupUsers();
        });
    };
    GroupUsersComponent.prototype.ngOnDestroy = function () {
        console.log('GroupUsersComponent: ngOnDestroy');
        this.sub.unsubscribe();
    };
    GroupUsersComponent.prototype.getGroupUsers = function () {
        var _this = this;
        console.log('GroupUsersComponent: getGroupUsers');
        this.userService
            .getGroupUsersById(this.groupId)
            .then(function (users) { return _this.users = users; });
    };
    GroupUsersComponent = __decorate([
        core_1.Component({
            selector: 'group-users-page',
            templateUrl: 'app/components/admin/groups/group.users/group.users.component.html',
            styleUrls: ['app/components/admin/groups/group.users/group.users.component.css'],
            providers: [user_service_1.UserService],
            directives: [users_table_component_1.UsersTableComponent]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, user_service_1.UserService])
    ], GroupUsersComponent);
    return GroupUsersComponent;
}());
exports.GroupUsersComponent = GroupUsersComponent;
//# sourceMappingURL=group.users.component.js.map
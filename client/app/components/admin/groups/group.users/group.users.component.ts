import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {UserService} from '../../../../shared/services/user.service';
import {IUser} from '../../../../shared/models/user.model';
import {UsersTableComponent} from '../../users/users.table/users.table.component';

@Component({
  selector: 'group-users-page',
  templateUrl: 'app/components/admin/groups/group.users/group.users.component.html',
  styleUrls: ['app/components/admin/groups/group.users/group.users.component.css'],
  providers: [UserService],
  directives: [UsersTableComponent]
})

export class GroupUsersComponent implements OnInit {
  users:IUser[];
  sub:any;
  groupId:string;

  constructor(private route:ActivatedRoute,
              private router:Router,
              private userService:UserService) {
    console.log('GroupUsersComponent: constructor');

  }

  ngOnInit() {
    console.log('GroupUsersComponent: ngOnInit');

    this.sub = this.router.routerState.parent(this.route).params.subscribe(params => {
      this.groupId = params['id'];
      this.getGroupUsers();
    });
  }

  ngOnDestroy() {
    console.log('GroupUsersComponent: ngOnDestroy');

    this.sub.unsubscribe();
  }

  getGroupUsers():void {
    console.log('GroupUsersComponent: getGroupUsers');

    this.userService
      .getGroupUsersById(this.groupId)
      .then((users:IUser[]) => this.users = users);
  }
}
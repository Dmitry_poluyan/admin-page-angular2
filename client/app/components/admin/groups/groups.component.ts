import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {IGroup} from '../../../shared/models/group.model';
import {GroupsTableComponent} from './groups.table/groups.table.component';
import {GroupService} from '../../../shared/services/group.service';

@Component({
  selector: 'groups-page',
  templateUrl: 'app/components/admin/groups/groups.component.html',
  styleUrls: ['app/components/admin/groups/groups.component.css'],
  directives: [GroupsTableComponent],
  providers: [GroupService]
})

export class GroupsComponent implements OnInit {
  groups:IGroup[];
  showDeleteBtn:boolean;

  constructor(private groupService:GroupService, private router:Router) {
    console.log('GroupsComponent: constructor');

    this.groups = [];
    this.showDeleteBtn = false;
  }

  ngOnInit() {
    console.log('GroupsComponent: ngOnInit');

    this.groupService
      .getGroups()
      .then(groups => this.groups = groups);
  }

  goToAddGroupPage() {
    console.log('UsersComponent: goToAddUserPage');

    this.router.navigate(['/admin/createGroup']);
  }
}



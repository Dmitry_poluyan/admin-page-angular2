"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var group_model_1 = require('../../../../shared/models/group.model');
var GroupFormComponent = (function () {
    function GroupFormComponent() {
        console.log('GroupFormComponent: constructor');
        this.submitted = new core_1.EventEmitter();
    }
    GroupFormComponent.prototype.submit = function (form) {
        console.log('GroupFormComponent: submit');
        if (this.model && form.valid) {
            this.submitted.emit(new group_model_1.Group(this.model));
        }
    };
    GroupFormComponent.prototype.goToBack = function () {
        console.log('GroupFormComponent: goBack');
        window.history.back();
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], GroupFormComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], GroupFormComponent.prototype, "nameSubmitBtn", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], GroupFormComponent.prototype, "submitted", void 0);
    GroupFormComponent = __decorate([
        core_1.Component({
            selector: 'group-form',
            templateUrl: 'app/components/admin/groups/group.form/group.form.component.html',
            styleUrls: ['app/components/admin/groups/group.form/group.form.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], GroupFormComponent);
    return GroupFormComponent;
}());
exports.GroupFormComponent = GroupFormComponent;
//# sourceMappingURL=group.form.component.js.map
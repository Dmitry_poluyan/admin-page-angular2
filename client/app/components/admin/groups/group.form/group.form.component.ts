import {Component, Input, Output, EventEmitter} from '@angular/core';
import {NgForm}    from '@angular/forms';

import {IGroup, Group} from '../../../../shared/models/group.model';

@Component({
  selector: 'group-form',
  templateUrl: 'app/components/admin/groups/group.form/group.form.component.html',
  styleUrls: ['app/components/admin/groups/group.form/group.form.component.css']
})

export class GroupFormComponent {
  @Input() model:IGroup;
  @Input() nameSubmitBtn:string;
  @Output() submitted:EventEmitter<IGroup>;

  constructor() {
    console.log('GroupFormComponent: constructor');

    this.submitted = new EventEmitter<IGroup>();
  }

  submit(form:NgForm) {
    console.log('GroupFormComponent: submit');

    if (this.model && form.valid) {
      this.submitted.emit(new Group(this.model));
    }
  }

  goToBack() {
    console.log('GroupFormComponent: goBack');

    window.history.back();
  }
}
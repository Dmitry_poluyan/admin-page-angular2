import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CORE_DIRECTIVES} from '@angular/common';
import {FORM_DIRECTIVES} from '@angular/forms';
import {PAGINATION_DIRECTIVES} from 'ng2-bootstrap/components/pagination';

import {UsersTableComponent} from './users.table/users.table.component';
import {IUser} from '../../../shared/models/user.model';
import {UserService} from '../../../shared/services/user.service';
import {SearchComponent} from '../../common/search/search.component';

@Component({
  selector: 'users-page',
  templateUrl: 'app/components/admin/users/users.component.html',
  styleUrls: ['app/components/admin/users/users.component.css'],
  directives: [UsersTableComponent, SearchComponent, PAGINATION_DIRECTIVES, FORM_DIRECTIVES, CORE_DIRECTIVES],
  providers: [UserService]
})

export class UsersComponent implements OnInit {
  users:IUser[];
  term:string;
  totalItems:number;
  currentPage:number;
  itemsPerPage:number;

  constructor(private userService:UserService, private router:Router) {
    console.log('UsersComponent: constructor');

    this.users = [];
    this.term = '';
    this.currentPage = 1;
  }

  ngOnInit() {
    console.log('UsersComponent: ngOnInit');

    this.getUsers(this.currentPage);
  }

  pageChanged(event:any):void {
    this.getUsers(event.page)
  };

  getUsers(page:number) {
    this.userService
      .getUsersByPageNumber(page)
      .then((res:any) => {
        this.users = res.users;
        this.totalItems = res.count;
        this.itemsPerPage = res.size;
      });
  }

  goToAddUserPage() {
    console.log('UsersComponent: goToAddUserPage');

    this.router.navigate(['/admin/createUser']);
  }

  onInputUpdated(value:string):void {
    console.log('UsersComponent: onInputUpdated');

    this.term = value;
  }
}



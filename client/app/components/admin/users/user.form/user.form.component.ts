import {Component, Input, Output, EventEmitter} from '@angular/core';
import {NgForm}    from '@angular/forms';

import {IUser, User} from '../../../../shared/models/user.model';

@Component({
  selector: 'user-form',
  templateUrl: 'app/components/admin/users/user.form/user.form.component.html',
  styleUrls: ['app/components/admin/users/user.form/user.form.component.css']
})

export class UserFormComponent {
  @Input() model:IUser;
  @Input() nameSubmitBtn:string;
  @Output() submitted:EventEmitter<IUser>;

  constructor() {
    console.log('UserFormComponent: constructor');

    this.submitted = new EventEmitter<IUser>();
  }

  submit(form:NgForm) {
    console.log('UserFormComponent: submit');

    let vm = this;

    if (this.model && form.valid) {
      this.submitted.emit(new User(vm.model));
    }
  }

  goToBack() {
    console.log('UserFormComponent: goBack');

    window.history.back();
  }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var user_model_1 = require('../../../../shared/models/user.model');
var UserFormComponent = (function () {
    function UserFormComponent() {
        console.log('UserFormComponent: constructor');
        this.submitted = new core_1.EventEmitter();
    }
    UserFormComponent.prototype.submit = function (form) {
        console.log('UserFormComponent: submit');
        var vm = this;
        if (this.model && form.valid) {
            this.submitted.emit(new user_model_1.User(vm.model));
        }
    };
    UserFormComponent.prototype.goToBack = function () {
        console.log('UserFormComponent: goBack');
        window.history.back();
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], UserFormComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], UserFormComponent.prototype, "nameSubmitBtn", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], UserFormComponent.prototype, "submitted", void 0);
    UserFormComponent = __decorate([
        core_1.Component({
            selector: 'user-form',
            templateUrl: 'app/components/admin/users/user.form/user.form.component.html',
            styleUrls: ['app/components/admin/users/user.form/user.form.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], UserFormComponent);
    return UserFormComponent;
}());
exports.UserFormComponent = UserFormComponent;
//# sourceMappingURL=user.form.component.js.map
import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {ToastyService} from 'ng2-toasty/ng2-toasty';

import {IUser, User} from '../../../../shared/models/user.model';
import {UserService} from '../../../../shared/services/user.service';
import {UserFormComponent} from '../user.form/user.form.component';

@Component({
  selector: 'create-user-page',
  templateUrl: 'app/components/admin/users/create.user/create.user.component.html',
  styleUrls: ['app/components/admin/users/create.user/create.user.component.css'],
  providers: [UserService],
  directives: [UserFormComponent]
})

export class CreateUserComponent {
  user:IUser;
  nameSubmitBtn:string;

  constructor(private userService:UserService,
              private toastyService:ToastyService,
              private router:Router) {
    console.log('CreateUserComponent: constructor');

    this.user = new User({});
    this.nameSubmitBtn = 'Create';
  }

  onUserCreated(user:IUser):void {
    console.log('CreateUserComponent: onUserCreated');

    if (user) {
      this.userService
        .createUser(this.user)
        .then(() => {
          this.toastyService.success({
            title: 'Success',
            msg: 'User added',
            theme: 'bootstrap'
          });

          this.user = new User({});
          this.goToUsersPage();
        }).catch(error => {

        this.toastyService.warning({
          title: 'Warning',
          msg: error ? error : 'Data is invalid',
          theme: 'bootstrap'
        });
      });
    }
  }

  private goToUsersPage():void {
    console.log('CreateUserComponent: goToAddUserPage');

    this.router.navigate(['/admin/users']);
  }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ng2_toasty_1 = require('ng2-toasty/ng2-toasty');
var user_model_1 = require('../../../../shared/models/user.model');
var user_service_1 = require('../../../../shared/services/user.service');
var user_form_component_1 = require('../user.form/user.form.component');
var CreateUserComponent = (function () {
    function CreateUserComponent(userService, toastyService, router) {
        this.userService = userService;
        this.toastyService = toastyService;
        this.router = router;
        console.log('CreateUserComponent: constructor');
        this.user = new user_model_1.User({});
        this.nameSubmitBtn = 'Create';
    }
    CreateUserComponent.prototype.onUserCreated = function (user) {
        var _this = this;
        console.log('CreateUserComponent: onUserCreated');
        if (user) {
            this.userService
                .createUser(this.user)
                .then(function () {
                _this.toastyService.success({
                    title: 'Success',
                    msg: 'User added',
                    theme: 'bootstrap'
                });
                _this.user = new user_model_1.User({});
                _this.goToUsersPage();
            }).catch(function (error) {
                _this.toastyService.warning({
                    title: 'Warning',
                    msg: error ? error : 'Data is invalid',
                    theme: 'bootstrap'
                });
            });
        }
    };
    CreateUserComponent.prototype.goToUsersPage = function () {
        console.log('CreateUserComponent: goToAddUserPage');
        this.router.navigate(['/admin/users']);
    };
    CreateUserComponent = __decorate([
        core_1.Component({
            selector: 'create-user-page',
            templateUrl: 'app/components/admin/users/create.user/create.user.component.html',
            styleUrls: ['app/components/admin/users/create.user/create.user.component.css'],
            providers: [user_service_1.UserService],
            directives: [user_form_component_1.UserFormComponent]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, ng2_toasty_1.ToastyService, router_1.Router])
    ], CreateUserComponent);
    return CreateUserComponent;
}());
exports.CreateUserComponent = CreateUserComponent;
//# sourceMappingURL=create.user.component.js.map
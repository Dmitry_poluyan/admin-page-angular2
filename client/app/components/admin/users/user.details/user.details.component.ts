import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {IUser, User} from '../../../../shared/models/user.model';
import {UserService} from '../../../../shared/services/user.service';

@Component({
  selector: 'user-details',
  templateUrl: 'app/components/admin/users/user.details/user.details.component.html',
  styleUrls: ['app/components/admin/users/user.details/user.details.component.css'],
  providers: [UserService]
})

export class UserDetailsComponent implements OnInit, OnDestroy {
  user:IUser;
  sub:any;

  constructor(private route:ActivatedRoute,
              private router:Router,
              private userService:UserService) {
    console.log('UserDetailsComponent: constructor');

    this.user = new User({});
  }

  ngOnInit() {
    console.log('UserDetailsComponent: ngOnInit');

    this.sub = this.router
      .routerState
      .parent(this.route)
      .params
      .subscribe(params => {
        let id = params['id'];

        this.userService
          .getUserById(id)
          .then((user:IUser) => this.user = user);
      });
  }

  ngOnDestroy() {
    console.log('UserDetailsComponent: ngOnDestroy');

    this.sub.unsubscribe();
  }
}
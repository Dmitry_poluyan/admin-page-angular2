"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var user_model_1 = require('../../../../shared/models/user.model');
var user_service_1 = require('../../../../shared/services/user.service');
var UserDetailsComponent = (function () {
    function UserDetailsComponent(route, router, userService) {
        this.route = route;
        this.router = router;
        this.userService = userService;
        console.log('UserDetailsComponent: constructor');
        this.user = new user_model_1.User({});
    }
    UserDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('UserDetailsComponent: ngOnInit');
        this.sub = this.router
            .routerState
            .parent(this.route)
            .params
            .subscribe(function (params) {
            var id = params['id'];
            _this.userService
                .getUserById(id)
                .then(function (user) { return _this.user = user; });
        });
    };
    UserDetailsComponent.prototype.ngOnDestroy = function () {
        console.log('UserDetailsComponent: ngOnDestroy');
        this.sub.unsubscribe();
    };
    UserDetailsComponent = __decorate([
        core_1.Component({
            selector: 'user-details',
            templateUrl: 'app/components/admin/users/user.details/user.details.component.html',
            styleUrls: ['app/components/admin/users/user.details/user.details.component.css'],
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, user_service_1.UserService])
    ], UserDetailsComponent);
    return UserDetailsComponent;
}());
exports.UserDetailsComponent = UserDetailsComponent;
//# sourceMappingURL=user.details.component.js.map
import {Component, Input} from '@angular/core';
import {ROUTER_DIRECTIVES}  from '@angular/router';

import {IUser} from '../../../../shared/models/user.model';
import {SearchPipe} from '../../../../shared/pipes/search.pipe';

@Component({
  selector: 'users-table',
  templateUrl: 'app/components/admin/users/users.table/users.table.component.html',
  styleUrls: ['app/components/admin/users/users.table/users.table.component.css'],
  directives: [ROUTER_DIRECTIVES],
  pipes: [SearchPipe]
})

export class UsersTableComponent {
  @Input() users:IUser[];
  @Input() term:string;

  constructor() {
    console.log('UsersTableComponent: constructor');
    this.term = '';
  }
}
import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {NgForm}    from '@angular/forms';

import {IGroup} from '../../../../shared/models/group.model';
import {GroupService} from '../../../../shared/services/group.service';

@Component({
  selector: 'add-user-group-form',
  templateUrl: 'app/components/admin/users/add.user.group.form/add.user.group.form.component.html',
  styleUrls: ['app/components/admin/users/add.user.group.form/add.user.group.form.component.css'],
  providers: [GroupService]
})

export class AddUserGroupFormComponent implements OnInit {
  @Output() submitted:EventEmitter<string>;
  groups:IGroup[];
  groupId:string;

  constructor(private groupService:GroupService) {
    console.log('AddUserGroupFormComponent: constructor');

    this.submitted = new EventEmitter<string>();
  }

  ngOnInit() {
    console.log('AddUserGroupFormComponent: ngOnInit');

    this.groupService
      .getGroups()
      .then((groups:IGroup[]) => this.groups = groups);
  }

  submit(form:NgForm) {
    console.log('AddUserGroupFormComponent: submit');

    if (this.groupId && form.valid) {
      this.submitted.emit(this.groupId);
    }
  }

  goToBack() {
    console.log('AddUserGroupFormComponent: goBack');

    window.history.back();
  }
}
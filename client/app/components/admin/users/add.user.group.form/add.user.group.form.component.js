"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var group_service_1 = require('../../../../shared/services/group.service');
var AddUserGroupFormComponent = (function () {
    function AddUserGroupFormComponent(groupService) {
        this.groupService = groupService;
        console.log('AddUserGroupFormComponent: constructor');
        this.submitted = new core_1.EventEmitter();
    }
    AddUserGroupFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('AddUserGroupFormComponent: ngOnInit');
        this.groupService
            .getGroups()
            .then(function (groups) { return _this.groups = groups; });
    };
    AddUserGroupFormComponent.prototype.submit = function (form) {
        console.log('AddUserGroupFormComponent: submit');
        if (this.groupId && form.valid) {
            this.submitted.emit(this.groupId);
        }
    };
    AddUserGroupFormComponent.prototype.goToBack = function () {
        console.log('AddUserGroupFormComponent: goBack');
        window.history.back();
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], AddUserGroupFormComponent.prototype, "submitted", void 0);
    AddUserGroupFormComponent = __decorate([
        core_1.Component({
            selector: 'add-user-group-form',
            templateUrl: 'app/components/admin/users/add.user.group.form/add.user.group.form.component.html',
            styleUrls: ['app/components/admin/users/add.user.group.form/add.user.group.form.component.css'],
            providers: [group_service_1.GroupService]
        }), 
        __metadata('design:paramtypes', [group_service_1.GroupService])
    ], AddUserGroupFormComponent);
    return AddUserGroupFormComponent;
}());
exports.AddUserGroupFormComponent = AddUserGroupFormComponent;
//# sourceMappingURL=add.user.group.form.component.js.map
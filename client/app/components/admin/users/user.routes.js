"use strict";
var create_user_component_1 = require('./create.user/create.user.component');
var profile_routes_1 = require('./user.profile/profile.routes');
exports.userRouter = [
    {
        path: 'createUser',
        component: create_user_component_1.CreateUserComponent
    }
].concat(profile_routes_1.profileRouter);
//# sourceMappingURL=user.routes.js.map
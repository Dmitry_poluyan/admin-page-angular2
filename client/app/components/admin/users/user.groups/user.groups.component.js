"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ng2_toasty_1 = require('ng2-toasty/ng2-toasty');
var group_service_1 = require('../../../../shared/services/group.service');
var user_service_1 = require('../../../../shared/services/user.service');
var groups_table_component_1 = require('../../groups/groups.table/groups.table.component');
var add_user_group_form_component_1 = require('../add.user.group.form/add.user.group.form.component');
var UserGroupsComponent = (function () {
    function UserGroupsComponent(route, router, userService, toastyService, groupService) {
        this.route = route;
        this.router = router;
        this.userService = userService;
        this.toastyService = toastyService;
        this.groupService = groupService;
        console.log('UserGroupsComponent: constructor');
        this.state = 'groupsList';
        this.showDeleteBtn = true;
    }
    UserGroupsComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('UserGroupsComponent: ngOnInit');
        this.sub = this.router.routerState.parent(this.route).params.subscribe(function (params) {
            _this.userId = params['id'];
            _this.getUserGroups();
        });
    };
    UserGroupsComponent.prototype.ngOnDestroy = function () {
        console.log('UserGroupsComponent: ngOnDestroy');
        this.sub.unsubscribe();
    };
    UserGroupsComponent.prototype.onUserGroupAdded = function (groupId) {
        var _this = this;
        console.log('UserGroupsComponent: onUserGroupAdded');
        if (groupId) {
            this.userService
                .addGroupForUser(groupId, this.userId)
                .then(function (res) {
                _this.state = 'groupsList';
                if (res.nModified > 0) {
                    _this.getUserGroups();
                    _this.toastyService.success({
                        title: 'Success',
                        msg: 'Group for added',
                        theme: 'bootstrap'
                    });
                }
            }).catch(function (error) {
                _this.toastyService.warning({
                    title: 'Warning',
                    msg: error ? error : 'Data is invalid',
                    theme: 'bootstrap'
                });
            });
        }
    };
    UserGroupsComponent.prototype.getUserGroups = function () {
        var _this = this;
        console.log('UserGroupsComponent: getUserGroups');
        this.groupService
            .getUserGroupsById(this.userId)
            .then(function (groups) { return _this.groups = groups; });
    };
    UserGroupsComponent.prototype.onGroupDeleted = function (group) {
        var _this = this;
        console.log('UserGroupsComponent: onGroupDeleted');
        if (group) {
            this.userService
                .deleteGroupFromUser(group._id, this.userId)
                .then(function (res) {
                if (res.nModified > 0) {
                    _this.getUserGroups();
                    _this.toastyService.success({
                        title: 'Success',
                        msg: 'Group from user deleted',
                        theme: 'bootstrap'
                    });
                }
            }).catch(function (error) {
                _this.toastyService.warning({
                    title: 'Warning',
                    msg: error ? error : 'Data is invalid',
                    theme: 'bootstrap'
                });
            });
        }
    };
    UserGroupsComponent = __decorate([
        core_1.Component({
            selector: 'user-groups-page',
            templateUrl: 'app/components/admin/users/user.groups/user.groups.component.html',
            styleUrls: ['app/components/admin/users/user.groups/user.groups.component.css'],
            providers: [group_service_1.GroupService],
            directives: [groups_table_component_1.GroupsTableComponent, add_user_group_form_component_1.AddUserGroupFormComponent]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, user_service_1.UserService, ng2_toasty_1.ToastyService, group_service_1.GroupService])
    ], UserGroupsComponent);
    return UserGroupsComponent;
}());
exports.UserGroupsComponent = UserGroupsComponent;
//# sourceMappingURL=user.groups.component.js.map
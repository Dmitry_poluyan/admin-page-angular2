import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastyService} from 'ng2-toasty/ng2-toasty';

import {GroupService} from '../../../../shared/services/group.service';
import {UserService} from '../../../../shared/services/user.service';
import {IGroup} from '../../../../shared/models/group.model';
import {GroupsTableComponent} from '../../groups/groups.table/groups.table.component';
import {AddUserGroupFormComponent} from '../add.user.group.form/add.user.group.form.component';

@Component({
  selector: 'user-groups-page',
  templateUrl: 'app/components/admin/users/user.groups/user.groups.component.html',
  styleUrls: ['app/components/admin/users/user.groups/user.groups.component.css'],
  providers: [GroupService],
  directives: [GroupsTableComponent, AddUserGroupFormComponent]
})

export class UserGroupsComponent implements OnInit {
  state:string;
  groups:IGroup[];
  sub:any;
  showDeleteBtn:boolean;
  userId:string;

  constructor(private route:ActivatedRoute,
              private router:Router,
              private userService:UserService,
              private toastyService:ToastyService,
              private groupService:GroupService) {
    console.log('UserGroupsComponent: constructor');

    this.state = 'groupsList';
    this.showDeleteBtn = true;
  }

  ngOnInit() {
    console.log('UserGroupsComponent: ngOnInit');

    this.sub = this.router.routerState.parent(this.route).params.subscribe(params => {
      this.userId = params['id'];
      this.getUserGroups();
    });
  }

  ngOnDestroy() {
    console.log('UserGroupsComponent: ngOnDestroy');

    this.sub.unsubscribe();
  }

  onUserGroupAdded(groupId:string):void {
    console.log('UserGroupsComponent: onUserGroupAdded');

    if (groupId) {
      this.userService
        .addGroupForUser(groupId, this.userId)
        .then((res:any) => {
          this.state = 'groupsList';

          if (res.nModified > 0) {
            this.getUserGroups();

            this.toastyService.success({
              title: 'Success',
              msg: 'Group for added',
              theme: 'bootstrap'
            });
          }
        }).catch((error:any) => {
        this.toastyService.warning({
          title: 'Warning',
          msg: error ? error : 'Data is invalid',
          theme: 'bootstrap'
        });
      });
    }
  }

  getUserGroups():void {
    console.log('UserGroupsComponent: getUserGroups');

    this.groupService
      .getUserGroupsById(this.userId)
      .then((groups:IGroup[]) => this.groups = groups);
  }

  onGroupDeleted(group:IGroup):void {
    console.log('UserGroupsComponent: onGroupDeleted');

    if (group) {
      this.userService
        .deleteGroupFromUser(group._id, this.userId)
        .then((res:any) => {

          if (res.nModified > 0) {
            this.getUserGroups();

            this.toastyService.success({
              title: 'Success',
              msg: 'Group from user deleted',
              theme: 'bootstrap'
            });
          }
        }).catch((error:any) => {
        this.toastyService.warning({
          title: 'Warning',
          msg: error ? error : 'Data is invalid',
          theme: 'bootstrap'
        });
      });
    }
  }
}
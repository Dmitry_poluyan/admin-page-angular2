"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ng2_toasty_1 = require('ng2-toasty/ng2-toasty');
var user_model_1 = require('../../../../shared/models/user.model');
var user_service_1 = require('../../../../shared/services/user.service');
var user_form_component_1 = require('../user.form/user.form.component');
var EditUserComponent = (function () {
    function EditUserComponent(route, router, userService, toastyService) {
        this.route = route;
        this.router = router;
        this.userService = userService;
        this.toastyService = toastyService;
        console.log('EditUserComponent: constructor');
        this.user = new user_model_1.User({});
        this.nameSubmitBtn = 'Edit';
    }
    EditUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('EditUserComponent: ngOnInit');
        this.sub = this.router
            .routerState
            .parent(this.route)
            .params
            .subscribe(function (params) {
            _this.userService
                .getUserById(params['id'])
                .then(function (user) { return _this.user = user; });
        });
    };
    EditUserComponent.prototype.onUserEdited = function (user) {
        var _this = this;
        console.log('EditUserComponent: onUserEdited');
        if (user) {
            this.userService
                .saveUser(this.user)
                .then(function () {
                _this.toastyService.success({
                    title: 'Success',
                    msg: 'User edited',
                    theme: 'bootstrap'
                });
                _this.goToBack();
            }).catch(function (error) {
                _this.toastyService.warning({
                    title: 'Warning',
                    msg: error ? error : 'Data is invalid',
                    theme: 'bootstrap'
                });
            });
        }
    };
    EditUserComponent.prototype.ngOnDestroy = function () {
        console.log('EditUserComponent: ngOnDestroy');
        this.sub.unsubscribe();
    };
    EditUserComponent.prototype.goToBack = function () {
        console.log('UserFormComponent: goBack');
        window.history.back();
    };
    EditUserComponent = __decorate([
        core_1.Component({
            selector: 'user-details',
            templateUrl: 'app/components/admin/users/edit.user/edit.user.component.html',
            styleUrls: ['app/components/admin/users/edit.user/edit.user.component.css'],
            providers: [user_service_1.UserService],
            directives: [user_form_component_1.UserFormComponent]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, user_service_1.UserService, ng2_toasty_1.ToastyService])
    ], EditUserComponent);
    return EditUserComponent;
}());
exports.EditUserComponent = EditUserComponent;
//# sourceMappingURL=edit.user.component.js.map
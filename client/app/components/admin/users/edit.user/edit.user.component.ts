import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastyService} from 'ng2-toasty/ng2-toasty';

import {IUser, User} from '../../../../shared/models/user.model';
import {UserService} from '../../../../shared/services/user.service';
import {UserFormComponent} from '../user.form/user.form.component';

@Component({
  selector: 'user-details',
  templateUrl: 'app/components/admin/users/edit.user/edit.user.component.html',
  styleUrls: ['app/components/admin/users/edit.user/edit.user.component.css'],
  providers: [UserService],
  directives: [UserFormComponent]
})

export class EditUserComponent implements OnInit, OnDestroy {
  user:IUser;
  sub:any;
  nameSubmitBtn:string;

  constructor(private route:ActivatedRoute,
              private router:Router,
              private userService:UserService,
              private toastyService:ToastyService) {
    console.log('EditUserComponent: constructor');

    this.user = new User({});
    this.nameSubmitBtn = 'Edit';
  }

  ngOnInit() {
    console.log('EditUserComponent: ngOnInit');

    this.sub = this.router
      .routerState
      .parent(this.route)
      .params
      .subscribe(params => {
        this.userService
          .getUserById(params['id'])
          .then((user:IUser) => this.user = user);
      });
  }

  onUserEdited(user:IUser) {
    console.log('EditUserComponent: onUserEdited');

    if (user) {
      this.userService
        .saveUser(this.user)
        .then(() => {
          this.toastyService.success({
            title: 'Success',
            msg: 'User edited',
            theme: 'bootstrap'
          });

          this.goToBack();
        }).catch((error:any) => {

        this.toastyService.warning({
          title: 'Warning',
          msg: error ? error : 'Data is invalid',
          theme: 'bootstrap'
        });
      });
    }
  }

  ngOnDestroy() {
    console.log('EditUserComponent: ngOnDestroy');

    this.sub.unsubscribe();
  }

  goToBack() {
    console.log('UserFormComponent: goBack');

    window.history.back();
  }
}
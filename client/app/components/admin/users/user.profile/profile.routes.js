"use strict";
var user_profile_component_1 = require('../user.profile/user.profile.component');
var user_details_component_1 = require('../user.details/user.details.component');
var edit_user_component_1 = require('../edit.user/edit.user.component');
var user_groups_component_1 = require('../user.groups/user.groups.component');
exports.profileRouter = [
    {
        path: 'userProfile/:id',
        component: user_profile_component_1.UserProfileComponent,
        children: [
            {
                path: '',
                redirectTo: 'details'
            },
            {
                path: 'details',
                component: user_details_component_1.UserDetailsComponent
            },
            {
                path: 'edit',
                component: edit_user_component_1.EditUserComponent
            },
            {
                path: 'userGroups',
                component: user_groups_component_1.UserGroupsComponent
            }
        ]
    }
];
//# sourceMappingURL=profile.routes.js.map
import {RouterConfig} from '@angular/router';

import {UserProfileComponent} from '../user.profile/user.profile.component';
import {UserDetailsComponent} from '../user.details/user.details.component';
import {EditUserComponent} from '../edit.user/edit.user.component';
import {UserGroupsComponent} from '../user.groups/user.groups.component';

export const profileRouter:RouterConfig = [
  {
    path: 'userProfile/:id',
    component: UserProfileComponent,
    children: [
      {
        path: '',
        redirectTo: 'details'
      },
      {
        path: 'details',
        component: UserDetailsComponent
      },
      {
        path: 'edit',
        component: EditUserComponent
      },
      {
        path: 'userGroups',
        component: UserGroupsComponent
      }
    ]
  }
];

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ng2_toasty_1 = require('ng2-toasty/ng2-toasty');
var user_service_1 = require('../../../../shared/services/user.service');
var UserProfileComponent = (function () {
    function UserProfileComponent(route, userService, router, toastyService) {
        this.route = route;
        this.userService = userService;
        this.router = router;
        this.toastyService = toastyService;
        console.log('UserProfileComponent: constructor');
    }
    UserProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('UserProfileComponent: ngOnInit');
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id'];
        });
    };
    UserProfileComponent.prototype.deleteUser = function () {
        var _this = this;
        console.log('UserProfileComponent: deleteUser');
        if (this.id && confirm('Are you sure?')) {
            this.userService
                .deleteUserById(this.id)
                .then(function () {
                _this.toastyService.success({
                    title: 'Success',
                    msg: 'User deleted',
                    theme: 'bootstrap'
                });
                _this.goToUsersPage();
            }).catch(function (error) {
                _this.toastyService.warning({
                    title: 'Warning',
                    msg: error ? error : 'Data is invalid',
                    theme: 'bootstrap'
                });
            });
        }
    };
    UserProfileComponent.prototype.goToUsersPage = function () {
        console.log('UserProfileComponent: goToUsersPage');
        this.router.navigate(['/admin/users']);
    };
    UserProfileComponent = __decorate([
        core_1.Component({
            selector: 'user-profile',
            templateUrl: 'app/components/admin/users/user.profile/user.profile.component.html',
            styleUrls: ['app/components/admin/users/user.profile/user.profile.component.css'],
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, user_service_1.UserService, router_1.Router, ng2_toasty_1.ToastyService])
    ], UserProfileComponent);
    return UserProfileComponent;
}());
exports.UserProfileComponent = UserProfileComponent;
//# sourceMappingURL=user.profile.component.js.map
import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, ActivatedRoute, Router}  from '@angular/router';
import {ToastyService} from 'ng2-toasty/ng2-toasty';

import {UserService} from '../../../../shared/services/user.service';

@Component({
  selector: 'user-profile',
  templateUrl: 'app/components/admin/users/user.profile/user.profile.component.html',
  styleUrls: ['app/components/admin/users/user.profile/user.profile.component.css'],
  directives: [ROUTER_DIRECTIVES],
  providers: [UserService]
})

export class UserProfileComponent {
  id:string;
  sub:any;

  constructor(private route:ActivatedRoute,
              private userService:UserService,
              private router:Router,
              private toastyService:ToastyService) {
    console.log('UserProfileComponent: constructor');
  }

  ngOnInit() {
    console.log('UserProfileComponent: ngOnInit');

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  deleteUser() {
    console.log('UserProfileComponent: deleteUser');

    if (this.id && confirm('Are you sure?')) {
      this.userService
        .deleteUserById(this.id)
        .then(() => {
          this.toastyService.success({
            title: 'Success',
            msg: 'User deleted',
            theme: 'bootstrap'
          });

          this.goToUsersPage();

        }).catch(error => {

        this.toastyService.warning({
          title: 'Warning',
          msg: error ? error : 'Data is invalid',
          theme: 'bootstrap'
        });
      });
    }
  }

  goToUsersPage() {
    console.log('UserProfileComponent: goToUsersPage');

    this.router.navigate(['/admin/users']);
  }
}



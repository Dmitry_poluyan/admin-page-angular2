import {RouterConfig} from '@angular/router';

import {CreateUserComponent} from './create.user/create.user.component';
import {profileRouter} from './user.profile/profile.routes';

export const userRouter:RouterConfig = [
  {
    path: 'createUser',
    component: CreateUserComponent
  },
  ...profileRouter
];
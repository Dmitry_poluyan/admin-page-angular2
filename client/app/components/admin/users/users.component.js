"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var pagination_1 = require('ng2-bootstrap/components/pagination');
var users_table_component_1 = require('./users.table/users.table.component');
var user_service_1 = require('../../../shared/services/user.service');
var search_component_1 = require('../../common/search/search.component');
var UsersComponent = (function () {
    function UsersComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        console.log('UsersComponent: constructor');
        this.users = [];
        this.term = '';
        this.currentPage = 1;
    }
    UsersComponent.prototype.ngOnInit = function () {
        console.log('UsersComponent: ngOnInit');
        this.getUsers(this.currentPage);
    };
    UsersComponent.prototype.pageChanged = function (event) {
        this.getUsers(event.page);
    };
    ;
    UsersComponent.prototype.getUsers = function (page) {
        var _this = this;
        this.userService
            .getUsersByPageNumber(page)
            .then(function (res) {
            _this.users = res.users;
            _this.totalItems = res.count;
            _this.itemsPerPage = res.size;
        });
    };
    UsersComponent.prototype.goToAddUserPage = function () {
        console.log('UsersComponent: goToAddUserPage');
        this.router.navigate(['/admin/createUser']);
    };
    UsersComponent.prototype.onInputUpdated = function (value) {
        console.log('UsersComponent: onInputUpdated');
        this.term = value;
    };
    UsersComponent = __decorate([
        core_1.Component({
            selector: 'users-page',
            templateUrl: 'app/components/admin/users/users.component.html',
            styleUrls: ['app/components/admin/users/users.component.css'],
            directives: [users_table_component_1.UsersTableComponent, search_component_1.SearchComponent, pagination_1.PAGINATION_DIRECTIVES, forms_1.FORM_DIRECTIVES, common_1.CORE_DIRECTIVES],
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
    ], UsersComponent);
    return UsersComponent;
}());
exports.UsersComponent = UsersComponent;
//# sourceMappingURL=users.component.js.map